import 'dart:typed_data';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';
import 'package:blue_thermal_printer/blue_thermal_printer.dart';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:print_bt_thermal/printenum.dart';

///Test printing
class TestPrint {
  BlueThermalPrinter bluetooth = BlueThermalPrinter.instance;

  sample() async {
    //image max 300px X 300px

    ///image from File path
    String filename = 'logo.png';
    ByteData bytesData = await rootBundle.load("assets/images/logo.png");
    String dir = (await getApplicationDocumentsDirectory()).path;
    File file = await File('$dir/$filename').writeAsBytes(bytesData.buffer
        .asUint8List(bytesData.offsetInBytes, bytesData.lengthInBytes));

    ///image from Asset
    ByteData bytesAsset = await rootBundle.load("assets/images/logolebar2.jpg");
    Uint8List imageBytesFromAsset = bytesAsset.buffer
        .asUint8List(bytesAsset.offsetInBytes, bytesAsset.lengthInBytes);

    ///image from Network
    var response = await http.get(Uri.parse(
        "https://raw.githubusercontent.com/kakzaki/blue_thermal_printer/master/example/assets/images/yourlogo.png"));
    Uint8List bytesNetwork = response.bodyBytes;
    Uint8List imageBytesFromNetwork = bytesNetwork.buffer
        .asUint8List(bytesNetwork.offsetInBytes, bytesNetwork.lengthInBytes);

    bluetooth.isConnected.then((isConnected) {
      if (isConnected == true) {
        //image from Asset
        bluetooth.printImageBytes(imageBytesFromAsset);
        bluetooth.printNewLine();
        bluetooth.printCustom(
            "PT. ASIA BANGUN ABADI", Size.boldMedium.val, Align.center.val);

        // bluetooth.printImage(file.path); //path of your image/logo
        bluetooth.printCustom("Telp.", Size.medium.val, Align.center.val);
        bluetooth.printCustom("No INVOICE", Size.medium.val, Align.center.val);
        bluetooth.printCustom("Tanggal : ", Size.medium.val, Align.center.val);

        // bluetooth.printImageBytes(imageBytesFromNetwork); //image from Network
        bluetooth.printNewLine();

        bluetooth.printLeftRight("BATA MERAH", "7.000.000", Size.medium.val,
            format: "%-20s %20s %n");
        bluetooth.printLeftRight("10 * @ 700.000,00", "", Size.bold.val,
            format: "%-20s %20s %n");
        bluetooth.printLeftRight("BATA RINGAN", "10.000.000", Size.medium.val,
            format: "%-20s %20s %n");
        bluetooth.printLeftRight("20 * @ 500.000,00", "", Size.bold.val,
            format: "%-20s %20s %n");

        bluetooth.printNewLine();
        bluetooth.printNewLine();

        bluetooth.print4Column("", "TOTAL BARANG", "25 Item", "", Size.bold.val,
            format: "%2s %14s %6s %16s %n");

        bluetooth.printNewLine();

        bluetooth.print4Column(
            "", "SUBTOTAL", "", "Rp. 10.000.000,00", Size.bold.val,
            format: "%1s %14s %6s %16s %n");
        // bluetooth.printCustom("čĆžŽšŠ-H-ščđ", Size.bold.val, Align.center.val,
        //     charset: "windows-1250");
        // bluetooth.printLeftRight("Številka:", "18000001", Size.bold.val,
        //     charset: "windows-1250");
        // bluetooth.printCustom("Body left", Size.bold.val, Align.left.val);
        // bluetooth.printCustom("Body right", Size.medium.val, Align.right.val);
        // bluetooth.printNewLine();
        // bluetooth.printCustom("Thank You", Size.bold.val, Align.center.val);
        // bluetooth.printNewLine();
        // bluetooth.printQRcode(
        //     "Insert Your Own Text to Generate", 200, 200, Align.center.val);
        // bluetooth.printNewLine();
        // bluetooth.printNewLine();
        bluetooth
            .paperCut(); //some printer not supported (sometime making image not centered)
        //bluetooth.drawerPin2(); // or you can use bluetooth.drawerPin5();
      }
    });
  }
}
